import XCTest

#if !os(macOS)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(TestLibrary2Tests.allTests),
    ]
}
#endif