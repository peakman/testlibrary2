import XCTest
@testable import TestLibrary2

final class TestLibrary2Tests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(TestLibrary2().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
