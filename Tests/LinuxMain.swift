import XCTest

import TestLibrary2Tests

var tests = [XCTestCaseEntry]()
tests += TestLibrary2Tests.allTests()
XCTMain(tests)